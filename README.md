# Homework Help

A git we will use to discuss your homework problems and look for solutions!
For quick questions, feel free to text me. It's always easier when I have the code in front of me, though! 



Here is a copy of my instructions from the original email: 

-----------------------------------------------------------------------------------
Let's say your homework is in ~/Desktop/code/homework1 and the homework-help repository is in ~/Desktop/code/homework-help. If you want to ask a question that involves your index.html and styles.css files, you can follow these steps to move and commit the git:

  cd ~/Desktop/code/homework-help/

  cp ~/Desktop/code/homework1/index.html .

  cp ~/Desktop/code/homework1/styles.css .

  git add --all

  git commit -m "Here are my homework files."

  git fetch

  git push

An explanation of those commands: The first line moves us to the homework-help repository directory. The second and third commands use 'cp' (copy) to copy the homework files to our current directory, '.' The fourth command adds all of the git files to the repository. The git commit/fetch/push are just regular git movement commands. 

Of course, modify the above commands to fit the proper directories for your files. I tested this, but not extensively, so let me know if you have any trouble or if you need more detailed instructions. 
---

